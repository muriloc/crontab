# CMS hourly.neon
5 0-3,6-23 * * * /usr/bin/php /var/www/html/cms/console/cm feefo:send_orders  && curl https://nosnch.in/56565d5b32 && curl https://mantle-reloaded.madesimplegroup.com/poke/feefo:send_orders@cms
10 0-3,6-23 * * * /usr/bin/php /var/www/html/cms/console/cm export:vo_services && curl https://nosnch.in/e8bf4720f2 && curl https://mantle-reloaded.madesimplegroup.com/poke/export:vo_services@cms
*/30 * * * * /usr/bin/php /var/www/html/cms/console/cm polling:poll_submissions && curl https://nosnch.in/138ee218a2 && curl https://mantle-reloaded.madesimplegroup.com/poke/polling:poll_submissions@cms
20 0-3,6-23 * * * /usr/bin/php /var/www/html/cms/console/cm services:process_upsells && curl https://nosnch.in/75f879fa00 && curl https://mantle-reloaded.madesimplegroup.com/poke/services:process_upsells@cms
*/30 0-3,6-23 * * * /usr/bin/php /var/www/html/cms/console/cm utils:feed_reader:fetch && curl https://nosnch.in/3576b0cb4c && curl https://mantle-reloaded.madesimplegroup.com/poke/utils:feed_reader:fetch@cms

# CMS daily.neon
01 7 * * * /usr/bin/php /var/www/html/cms/console/cm psc:sweeper && curl https://nosnch.in/5197abc0d3 && curl https://mantle-reloaded.madesimplegroup.com/poke/psc:sweeper@cms
01 7 * * * /usr/bin/php /var/www/html/cms/console/cm cashplus:load_products && curl https://nosnch.in/a53cea3508 && curl https://mantle-reloaded.madesimplegroup.com/poke/cashplus:load_products@cms
01 1,8 * * * /usr/bin/php /var/www/html/cms/console/cm feefo:update_status && curl https://nosnch.in/d288562c68 && curl https://mantle-reloaded.madesimplegroup.com/poke/feefo:update_status@cms
01 1,8 * * * /usr/bin/php /var/www/html/cms/console/cm reminders:tokens && curl https://nosnch.in/7afb28f680 && curl https://mantle-reloaded.madesimplegroup.com/poke/reminders:tokens@cms
01 1,8 * * * /usr/bin/php /var/www/html/cms/console/cm services:renewals:disable --dryRun false && curl https://nosnch.in/4945358d39 && curl https://mantle-reloaded.madesimplegroup.com/poke/services:renewals:disable@cms
30 1,8 * * * /usr/bin/php /var/www/html/cms/console/cm services:reminders:send_auto && curl https://nosnch.in/14c2fe709a && curl https://mantle-reloaded.madesimplegroup.com/poke/services:reminders:send_auto@cms
40 1,8 * * * /usr/bin/php /var/www/html/cms/console/cm services:renewals:process --dryRun false && curl https://nosnch.in/6d0778006e && curl https://mantle-reloaded.madesimplegroup.com/poke/services:renewals:process@cms
01 1,8 * * * /usr/bin/php /var/www/html/cms/console/cm offers:send_csv && curl https://nosnch.in/b2af4cd8be && curl https://mantle-reloaded.madesimplegroup.com/poke/offers:send_csv@cms
#01 1,8 * * * /usr/bin/php /var/www/html/cms/console/cm offers:worldpay:send_leads
#01 1,8 * * * /usr/bin/php /var/www/html/cms/console/cm id:old_reminders
01 1,8 * * * /usr/bin/php /var/www/html/cms/console/cm services:reminders && curl https://nosnch.in/1e89ae397a && curl https://mantle-reloaded.madesimplegroup.com/poke/services:reminders@cms
01 1,8 * * * /usr/bin/php /var/www/html/cms/console/cm export:fraud_protection_companies && curl https://nosnch.in/0c7aab8fa1 && curl https://mantle-reloaded.madesimplegroup.com/poke/export:fraud_protection_companies@cms
01 1,8 * * * /usr/bin/php /var/www/html/cms/console/cm feedback:update && curl https://nosnch.in/095e91bd72 && curl https://mantle-reloaded.madesimplegroup.com/poke/feedback:update@cms

#Cashplus leads
0 */8 * * * /usr/bin/php /var/www/html/cms/console/cm cron:business_services:cashplus:submit_leads --dryRun=false && curl https://nosnch.in/2e693bf985 && curl https://mantle-reloaded.madesimplegroup.com/poke/cron:business_services:cashplus:submit_leads@cms

# Services
00 9 * * * /usr/bin/php /var/www/html/cms/console/cm services:education_emails && curl https://nosnch.in/4df863d4da && curl https://mantle-reloaded.madesimplegroup.com/poke/services:education_emails@cms

# Banking Sweeper
01 9 * * * /usr/bin/php /var/www/html/cms/console/cm cron:banking_module:sweeper && curl https://nosnch.in/db5ec24388 && curl https://mantle-reloaded.madesimplegroup.com/poke/cron:banking_module:sweeper@cms

# CMS toolkit_offers.neon
# 1 7 * * * /usr/bin/php /var/www/html/cms/console/cm toolkit_offers:process:namesco
# 1 7 * * * /usr/bin/php /var/www/html/cms/console/cm toolkit_offers:process:adwords && curl https://nosnch.in/194b72e1cb
1 7 * * * /usr/bin/php /var/www/html/cms/console/cm toolkit_offers:process:tax_assist && curl https://nosnch.in/e376032565 && curl https://mantle-reloaded.madesimplegroup.com/poke/toolkit_offers:process:tax_assist@cms
1 7 * * * /usr/bin/php /var/www/html/cms/console/cm toolkit_offers:process:free_agent && curl https://nosnch.in/35bfc7b85c && curl https://mantle-reloaded.madesimplegroup.com/poke/toolkit_offers:process:free_agent@cms
1 7 * * * /usr/bin/php /var/www/html/cms/console/cm toolkit_offers:process:ebook && curl https://nosnch.in/30dc149690 && curl https://mantle-reloaded.madesimplegroup.com/poke/toolkit_offers:process:ebook@cms
# 1 7 * * * /usr/bin/php /var/www/html/cms/console/cm toolkit_offers:process:facebook

# Namesco enabled by default
# OLD 1 21 * * * /usr/bin/php /var/www/html/cms/console/cm marketing:namesco_csv:export
0 10 * * * /usr/bin/php /var/www/html/cms/console/cm marketing:namesco_voucher_emailer && curl https://nosnch.in/f71d91d748 && curl https://mantle-reloaded.madesimplegroup.com/poke/marketing:namesco_voucher_emailer@cms

#1 7 * * * /usr/bin/php /var/www/html/cms/console/cm toolkit_offers:work_hub_offers
22 7 * * * /usr/bin/php /var/www/html/cms/console/cm toolkit_offers:process_types --types=moneypenny && curl https://nosnch.in/6f841cae58 && curl https://mantle-reloaded.madesimplegroup.com/poke/toolkit_offers:process_types@cms

# CMS tsb_banking.neon
# 1 0-3,6-23 * * * /usr/bin/php /var/www/html/cms/console/cm cron:run --config tsb_banking.neon
# 0 */8 * * * /usr/bin/php /var/www/html/cms/console/cm banking:tsb:leads && curl https://nosnch.in/cd3bc0e9cb && curl https://mantle-reloaded.madesimplegroup.com/poke/banking:tsb:leads@cms
# 0 6 * * 1 /usr/bin/php /var/www/html/cms/console/cm banking:tsb:weekly_summary && curl https://nosnch.in/69f106a152 && curl https://mantle-reloaded.madesimplegroup.com/poke/banking:tsb:weekly_summary@cms
# 0 6 1 * * /usr/bin/php /var/www/html/cms/console/cm banking:tsb:monthly_summary && curl https://nosnch.in/93c69b8a1d && curl https://mantle-reloaded.madesimplegroup.com/poke/banking:tsb:monthly_summary@cms
# 1 0 * * * /usr/bin/php /var/www/html/cms/console/cm banking:tsb:application_reminders && curl https://nosnch.in/8aa89df590 && curl https://mantle-reloaded.madesimplegroup.com/poke/banking:tsb:application_reminders@cms

*/15 0-3,6-23 * * * /var/www/html/cms/vendor/bin/behat --config /var/www/html/cms/vendor/made_simple/msg-framework/workflow_engine_module/src/WorkflowEngineModule/Config/workflow_engine.yml --tags="~test&&~disabled" >> /var/www/html/cms/logs/workflow_engine/output.log && curl https://nosnch.in/e1bf3fa7a1 && curl https://mantle-reloaded.madesimplegroup.com/poke/behat-workflow_engine@cms

#Cleaning script
01 0 1 * * /usr/bin/php /var/www/html/cms/console/cm clean:envelopes && curl https://nosnch.in/0eef9be01e && curl https://mantle-reloaded.madesimplegroup.com/poke/clean:envelopes@cms

#Tide
2 0-3,6-23 * * * /usr/bin/php /var/www/html/cms/console/cm banking:tide:send_applications && curl https://nosnch.in/fe63e05468 && curl https://mantle-reloaded.madesimplegroup.com/poke/banking:tide:send_applications@cms

#Digital mailroom
0 0-3,6-23 * * * /usr/bin/php /var/www/html/cms/console/cm cron:export:company_names && curl https://nosnch.in/8561ac5727 && curl https://mantle-reloaded.madesimplegroup.com/poke/cron:export:company_names@cms
20 8-18 * * 1-5 /usr/bin/php /var/www/html/cms/console/cm cron:kofax:process_scans --debug true && curl https://nosnch.in/1e2cffe552 && curl https://mantle-reloaded.madesimplegroup.com/poke/cron:kofax:process_scans@cms

## id validation
01 3,20 * * * /usr/bin/php /var/www/html/cms/console/cm cron:id:record_company_status && curl https://nosnch.in/57178b98fd && curl https://mantle-reloaded.madesimplegroup.com/poke/cron:id:record_company_status@cms
#11 6,14,20 * * * /usr/bin/php /var/www/html/cms/console/cm cron:id:nullify --ignoreDiacritics=true

51 7,15,21 * * * /usr/bin/php /var/www/html/cms/console/cm cron:id:auto_checker && curl https://nosnch.in/79545a2340 && curl https://mantle-reloaded.madesimplegroup.com/poke/cron:id:auto_checker@cms

## expire customers credits
0 0 1 * * /usr/bin/php /var/www/html/cms/console/cm cron:customer:expireCredit && curl https://nosnch.in/f79320e7e5 && curl https://mantle-reloaded.madesimplegroup.com/poke/cron:customer:expireCredit@cms


# company summary
03 6 * * * /usr/bin/php /var/www/html/cms/console/cm cron:id:company_summary_emails --dateFrom "yesterday" && curl https://nosnch.in/88fba97d10 && curl https://mantle-reloaded.madesimplegroup.com/poke/cron:id:company_summary_emails@cms

# summary
23 6 * * * /usr/bin/php /var/www/html/cms/console/cm cron:id:summary_emails --dateTo="-6 days" && curl https://nosnch.in/3f98031d89 && curl https://mantle-reloaded.madesimplegroup.com/poke/cron:id:summary_emails@cms

# status email
31 20 * * * /usr/bin/php /var/www/html/cms/console/cm cron:id:status_emails --dateFrom "yesterday" && curl https://nosnch.in/6e6eb88015 && curl https://mantle-reloaded.madesimplegroup.com/poke/cron:id:status_emails@cms

# eFiling submit leads
45 0-3,6-23 * * * /usr/bin/php /var/www/html/cms/console/cm cron:id:business_services:efiling:submit_leads && curl https://nosnch.in/e8c52cd327 && curl https://mantle-reloaded.madesimplegroup.com/poke/cron:id:business_services:efiling:submit_leads@cms

# FreeAgent submit leads
50 0-3,6-23 * * * /usr/bin/php /var/www/html/cms/console/cm cron:business_services:freeagent:submit_leads && curl https://nosnch.in/6925eb8d58 && curl https://mantle-reloaded.madesimplegroup.com/poke/cron:business_services:freeagent:submit_leads@cms

# WiseBusiness former transferwise
01 9 * * * /usr/bin/php /var/www/html/cms/console/cm cron:business_services:wise_business:send_customers_emails --dryRun=false && curl https://nosnch.in/66857655e3 && curl https://mantle-reloaded.madesimplegroup.com/poke/cron:business_services:wise_business:send_customers_emails@cms
05 9 * * 1 /usr/bin/php /var/www/html/cms/console/cm cron:business_services:wise_business:submit_leads --dryRun=false && curl https://nosnch.in/a42eae5a98 && curl https://mantle-reloaded.madesimplegroup.com/poke/cron:business_services:wise_business:submit_leads@cms


# resubmit error envelopes
0 0-3,6-23 * * * /usr/bin/php /var/www/html/cms/console/cm resubmit:incorporations && curl https://nosnch.in/a2dfec2f6e && curl https://mantle-reloaded.madesimplegroup.com/poke/resubmit:incorporations@cms

# submission queue reminders and cleaning
1 0-3,6-23 * * * /usr/bin/php /var/www/html/cms/console/cm submission:queue:send_reminders && curl https://nosnch.in/cff9f86060 && curl https://mantle-reloaded.madesimplegroup.com/poke/submission:queue:send_reminders@cms

# brookson
#45 8 * * * /usr/bin/php /var/www/html/cms/console/cm cron:business_services:brookson:submit_leads

# cashback main
6 19 * * * /usr/bin/php /var/www/html/cms/console/cm cashbacks:main && curl https://nosnch.in/e0d904e664 && curl https://mantle-reloaded.madesimplegroup.com/poke/cashbacks:main@cms

# tax assist leads
0 9 * * * /usr/bin/php /var/www/html/cms/console/cm cron:business_services:tax_assist:submit_leads && curl https://nosnch.in/c6c6de2bb0 && curl https://mantle-reloaded.madesimplegroup.com/poke/cron:business_services:tax_assist:submit_leads@cms


# worldpay leads
#30 1,3,6,7 * * * /usr/bin/php /var/www/html/cms/console/cm cron:business_services:worldpay:submit_leads

# iwoca opt in emails
0 7 * * * /usr/bin/php /var/www/html/cms/console/cm offers:iwoca:send_leads && curl https://nosnch.in/f8a37323ec && curl https://mantle-reloaded.madesimplegroup.com/poke/offers:iwoca:send_leads@cms

# Pearl chartered
#30 9-17 * * * /usr/bin/php /var/www/html/cms/console/cm business_services:pearl_chartered:submit_leads
30 9-17 * * * /usr/bin/php /var/www/html/cms/console/cm toolkit_offers:process:pearl && curl https://nosnch.in/2bb881b8c9 && curl https://mantle-reloaded.madesimplegroup.com/poke/toolkit_offers:process:pearl@cms
30 9-17 * * * /usr/bin/php /var/www/html/cms/console/cm offers:pearl:send_leads && curl https://nosnch.in/849c1bb3c9 && curl https://mantle-reloaded.madesimplegroup.com/poke/offers:pearl:send_leads@cms

# bdg leads
0 0-3,6-23 * * * /usr/bin/php /var/www/html/cms/console/cm cron:business_services:bdg:submit_leads --dryRun=false && curl https://nosnch.in/f42faceb1e && curl https://mantle-reloaded.madesimplegroup.com/poke/cron:business_services:bdg:submit_leads@cms
0 8 * * * /usr/bin/php /var/www/html/cms/console/cm cron:business_services:bdg:pending_incorporated_leads && curl https://nosnch.in/820bb8d50e && curl https://mantle-reloaded.madesimplegroup.com/poke/cron:business_services:bdg:pending_incorporated_leads@cms

# workhub free day pass
#01 8 * * * /usr/bin/php /var/www/html/cms/console/cm offers:workhub:free_day_send_email

# remove expired auth tokens
14 7 * * * /usr/bin/php /var/www/html/cms/console/cm user_module:remove_expired_auth_tokens_command && curl https://nosnch.in/23f6b482c9 && curl https://mantle-reloaded.madesimplegroup.com/poke/user_module:remove_expired_auth_tokens_command@cms

# takepayments leads
0 13 * * * /usr/bin/php /var/www/html/cms/console/cm cron:business_services:takepayments:submit_leads && curl https://nosnch.in/2c9630a86e && curl https://mantle-reloaded.madesimplegroup.com/poke/cron:business_services:takepayments:submit_leads@cms

# abandoned basket
0 0-3,6-23 * * * /usr/bin/php /var/www/html/cms/console/cm services:reminders:abandoned_basket && curl https://nosnch.in/36d86f9905 && curl https://mantle-reloaded.madesimplegroup.com/poke/services:reminders:abandoned_basket@cms

# mettle leads
0 */8 * * * /usr/bin/php /var/www/html/cms/console/cm cron:business_services:mettle:submit_leads --dryRun false && curl https://nosnch.in/af58a2a157 && curl https://mantle-reloaded.madesimplegroup.com/poke/cron:business_services:mettle:submit_leads@cms

# touch
0 */8 * * * /usr/bin/php /var/www/html/cms/console/cm cron:business_services:touch:submit_leads && curl https://nosnch.in/e872a301d1 && curl https://mantle-reloaded.madesimplegroup.com/poke/cron:business_services:touch:submit_leads@cms


# Anna
0 */8 * * * /usr/bin/php /var/www/html/cms/console/cm cron:business_services:anna:submit_leads --dryRun false && curl https://nosnch.in/d1d82908b1 && curl https://mantle-reloaded.madesimplegroup.com/poke/cron:business_services:anna:submit_leads@cms
30 */8 * * * /usr/bin/php /var/www/html/cms/console/cm cron:business_services:anna:submit_package_leads --dryRun false && curl https://nosnch.in/f34bd9709c && curl https://mantle-reloaded.madesimplegroup.com/poke/cron:business_services:anna:submit_package_leads@cms
0 18 * * * /usr/bin/php /var/www/html/cms/console/cm cron:business_services:anna:submit_update_status --dryRun false && curl https://nosnch.in/f3bde17333 && curl https://mantle-reloaded.madesimplegroup.com/poke/cron:business_services:anna:submit_update_status@cms

# Crunch
0 */8 * * * /usr/bin/php /var/www/html/cms/console/cm cron:business_services:crunch:submit_leads && curl https://nosnch.in/16bbff7706 && curl https://mantle-reloaded.madesimplegroup.com/poke/cron:business_services:crunch:submit_leads@cms

# Web.com
0 */8 * * * /usr/bin/php /var/www/html/cms/console/cm cron:business_services:webodotcom:submit_leads && curl https://nosnch.in/ebbe5a46b0 && curl https://mantle-reloaded.madesimplegroup.com/poke/cron:business_services:webodotcom:submit_leads@cms

# Other company documents - move to GCP
1 1 2 * * /usr/bin/php /var/www/html/cms/console/cm cron:company_other_documents:move

# Payoneer
0 18 * * * /usr/bin/php /var/www/html/cms/console/cm cron:business_services:payoneer:submit_leads --dryRun=false && curl https://nosnch.in/0167ea010a && curl https://mantle-reloaded.madesimplegroup.com/poke/cron:business_services:payoneer:submit_leads@cms
6 * * * * /usr/bin/php /var/www/html/cms/console/cm cron:business_services:payoneer:send_customers_emails --dryRun=false && curl https://nosnch.in/f5108d4fef && curl https://mantle-reloaded.madesimplegroup.com/poke/cron:business_services:payoneer:send_customers_emails@cms

# Pending Submissions on Queue (ID CHECK)
0 * * * * /usr/bin/php /var/www/html/cms/console/cm submission:queue:process_ready_submissions --dryRun=false && curl https://nosnch.in/aab60cc36c && curl https://mantle-reloaded.madesimplegroup.com/poke/submission:queue:process_ready_submissions@cms

# MoneyPenny Numbers
0 * * * * /usr/bin/php /var/www/html/cms/console/cm cron:money_penny:create_accounts --dryRun=false && curl https://nosnch.in/44fc8f6944 && curl https://mantle-reloaded.madesimplegroup.com/poke/cron:money_penny:create_accounts@cms
0 0 1 * * /usr/bin/php /var/www/html/cms/console/cm cron:money_penny:update_prefixes --dryRun=false && curl https://nosnch.in/7fc7e6e98e && curl https://mantle-reloaded.madesimplegroup.com/poke/cron:money_penny:update_prefixes@cms
01 18 * * * /usr/bin/php /var/www/html/cms/console/cm cron:products:seal_and_stamp_supplier --dryRun=false && curl https://nosnch.in/035c5c5000 && curl https://mantle-reloaded.madesimplegroup.com/poke/cron:products:seal_and_stamp_supplier@cms

# Xeinadin Leads
0 9 * * * /usr/bin/php /var/www/html/cms/console/cm offers:xeinadin:send_leads --dryRun false && curl https://nosnch.in/f4c84848ab && curl https://mantle-reloaded.madesimplegroup.com/poke/offers:xeinadin:send_leads@cms

# Yell Leads
0 9 * * * /usr/bin/php /var/www/html/cms/console/cm offers:yell:send_leads --dryRun false && curl https://nosnch.in/d9bfbe7b69 && curl https://mantle-reloaded.madesimplegroup.com/poke/offers:yell:send_leads@cms

# WorkHub Pass
1 8 * * * /usr/bin/php /var/www/html/cms/console/cm products:ultimate:workhub_passes --dryRun false
